<?php namespace App\Controllers;


class User extends BaseController{

	public function index()
	{
		return view('login/view');
	}
 
	public function doLogin(){

		$emailData = $this->request->getVar('email');

		// validate
		$this->form_validation->setRule('email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->setRule('password', 'Password', 'trim|required|xss_clean');

		//data from input
		$data = array(
			'email' => $emailData,
			'password' =>password_hash( $this->request->getVar('password'),PASSWORD_DEFAULT)
			);

		// call model to check the user	
		$result = $this->login->checkUser($data);

		if($result){
	
			$username = $emailData;
			if ($result != false) {
				$session_data = array(
				'email' => $result[0]->email
				);

			}
			// Add user data in session
			$this->session->set_userdata('logged_in', $session_data);
			
			// redirect to contact list
			echo view('login/view',$data);

		}else {

			$data = array(
			'error_message' => 'Invalid email or password'
			);

			echo view('login/view',$data);
		}
	
		


	}

	//--------------------------------------------------------------------

}
